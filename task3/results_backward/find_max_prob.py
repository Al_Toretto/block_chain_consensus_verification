#!/bin/python3
import json

with open('results.json', 'r') as f:
    data = json.load(f)

data = data["modelCheckResultList"]
model_with_max_prob = data[0]

for model in data:
    if (model["probability"] > model_with_max_prob["probability"] or
     (model["probability"] == model_with_max_prob["probability"] and model["expectedMessages"] < model_with_max_prob["expectedMessages"])):
        model_with_max_prob = model
print(model_with_max_prob)