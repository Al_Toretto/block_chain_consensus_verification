import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import services.SenderService;
import services.SendingConfirmationService;
import services.SendingConfirmationServiceImpl;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SendingConfirmationServiceImplTest {

    static SendingConfirmationService confirmationService;

    public SendingConfirmationServiceImplTest() {}

    @BeforeAll
    static void init() {
        confirmationService = new SendingConfirmationServiceImpl();
    }

    @Test
    public void sendCustomIdTest() {
        boolean consensusReached = confirmationService.sendForConfirmationCustomId(new SenderServiceImpl(),
                "src/test/resources/results_task3_no_backward.json",
                799820985);

        assertTrue(consensusReached);
    }
    static class SenderServiceImpl implements SenderService {

        private boolean getResponse(double probability) {
            return ThreadLocalRandom.current().nextFloat() <= probability;
        }

        @Override
        public int getReply(String orgName, int transactionId) {
            HashMap<String, Double> probabilities = new HashMap<>();
            probabilities.put("Org1", 0.995);
            probabilities.put("Org2", 0.99);
            probabilities.put("Org3", 0.985);
            probabilities.put("Org4", 0.988);
            return (getResponse(probabilities.get(orgName)) ? 1 : 0);
        }

        @Override
        public int getMaxRequestNum() {
            return 10;
        }

        @Override
        public int getMaxRequestTotalNum() {
            return 50;
        }

        @Override
        public long getTimeoutSec() {
            return 0;
        }

        @Override
        public long getWaitingTimeSec() {
            return 0;
        }

        @Override
        public String getLogPath() {
            return "src/test/resources/scheduler.log";
        }
    }
}
